O:Today we learned the basic knowledge of databases and JPA related knowledge. In fact, based on yesterday's project, the original simulated database was changed to a real database. JPA is a Java EE specification based on ORM technology. We also learned Spring Data JPA, a module under the Spring framework that is based on the upper layer encapsulation of the JPA specification.

R:I feel a bit unfamiliar.

I:In previous projects, I used mybatis for database interaction. So I am not very familiar with Spring JPA. Today's course taught me a new type of ORM.

D:I hope to have a deeper understanding of Spring JPA, an ORM, and compare it with mybatis. This not only enables learning new knowledge, but also consolidates old knowledge and provides a deeper understanding of when and what kind of ORM should be used.