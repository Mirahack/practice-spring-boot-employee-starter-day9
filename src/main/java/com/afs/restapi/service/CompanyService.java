package com.afs.restapi.service;

import com.afs.restapi.entity.Company;
import com.afs.restapi.repository.CompanyRepository;
import com.afs.restapi.repository.EmployeeRepository;
import com.afs.restapi.entity.Employee;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompanyService {

    private final CompanyRepository companyRepository;
    private final EmployeeRepository employeeRepository;

    public CompanyService(CompanyRepository companyRepository, EmployeeService employeeService, EmployeeRepository employeeRepository) {
        this.companyRepository = companyRepository;
        this.employeeRepository = employeeRepository;
    }

    public List<Company> findAll() {
        return companyRepository.findAll();
    }

    public List<Company> findByPage(Integer page, Integer size) {
        return companyRepository.findAll(PageRequest.of(page - 1, size)).getContent();
    }

    public Company findById(Long id) {
        return companyRepository.findById(id).orElse(null);
    }

    public void update(Long id, Company company) {
        companyRepository.findById(id).ifPresent(originCompany -> {
            originCompany.setName(company.getName());
            companyRepository.save(originCompany);
        });
    }

    public Company create(Company company) {
        return companyRepository.save(company);
    }

    public List<Employee> findEmployeesByCompanyId(Long id) {
        return employeeRepository.findByCompanyId(id);
    }

    public void delete(Long id) {
        companyRepository.deleteById(id);
    }
}
